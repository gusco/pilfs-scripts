#!/bin/bash
#
# PiLFS Build Script for LFS Version r11.3-223
# Builds Weston
# https://intestinate.com/pilfs
#
# Optional parameters below:

RPI_MODEL=64                    # Which Raspberry Pi model are you building for - this selects the right GCC CPU patch.
                                # Put 64 to build for aarch64.
PARALLEL_JOBS=4                 # Number of parallel make jobs, 1 for RPi1 and 4 for RPi2 and up recommended.

# End of optional parameters

set -o nounset
set -o errexit

function prebuild_sanity_check {
    if [[ $(whoami) != "root" ]] ; then
        echo "You should be running as root for the post build!"
        exit 1
    fi

    if ! [[ -d /usr/src ]] ; then
        echo "Can't find your /usr/src directory! Did you forget to chroot?"
        exit 1
    fi
}

function check_tarballs {
LIST_OF_TARBALLS="
libxml2-2.10.4.tar.xz
libpng-1.6.40.tar.xz
libpng-1.6.39-apng.patch.gz
libuv-v1.46.0.tar.gz
libarchive-3.7.1.tar.xz
cmake-3.27.2.tar.gz
libjpeg-turbo-3.0.0.tar.gz
pixman-0.42.2.tar.gz
glib-2.76.4.tar.xz
glib-skip_warnings-1.patch
freetype-2.13.1.tar.xz
fontconfig-2.14.2.tar.xz
libdrm-2.4.115.tar.xz
wayland-1.22.0.tar.xz
wayland-protocols-1.32.tar.xz
wayland-utils-1.2.0.tar.xz
Mako-1.2.4.tar.gz
mesa-23.1.6.tar.xz
cairo-1.17.6.tar.xz
mtdev-1.1.6.tar.bz2
mtdev-1.1.6-aarch64-fix.patch
xkeyboard-config-2.39.tar.xz
libxkbcommon-1.5.0.tar.xz
libevdev-1.13.1.tar.xz
libinput-1.21.0.tar.xz
0.8.0.tar.gz
weston-12.0.2.tar.xz
dejavu-fonts-ttf-2.37.tar.bz2
"

for tarball in $LIST_OF_TARBALLS ; do
    if ! [[ -f /usr/src/$tarball ]] ; then
        echo "Can't find /usr/src/$tarball!"
        exit 1
    fi
done
}

function timer {
    if [[ $# -eq 0 ]]; then
        echo $(date '+%s')
    else
        local stime=$1
        etime=$(date '+%s')
        if [[ -z "$stime" ]]; then stime=$etime; fi
        dt=$((etime - stime))
        ds=$((dt % 60))
        dm=$(((dt / 60) % 60))
        dh=$((dt / 3600))
        printf '%02d:%02d:%02d' $dh $dm $ds
    fi
}

prebuild_sanity_check
check_tarballs

echo -e "\nThis is your last chance to quit before we start building... continue?"
echo "(Note that if anything goes wrong during the build, the script will abort mission)"
select yn in "Yes" "No"; do
    case $yn in
        Yes) break;;
        No) exit;;
    esac
done

total_time=$(timer)

echo "# Moving on to /usr/src"
cd /usr/src

echo "# libxml2-2.10.4"
tar -Jxf libxml2-2.10.4.tar.xz
cd libxml2-2.10.4
./configure --prefix=/usr           \
            --sysconfdir=/etc       \
            --disable-static        \
            --with-history          \
            PYTHON=/usr/bin/python3 \
            --docdir=/usr/share/doc/libxml2-2.10.4
make -j $PARALLEL_JOBS
make install
cd /usr/src
rm -rf libxml2-2.10.4

echo "# libpng-1.6.40"
tar -Jxf libpng-1.6.40.tar.xz
cd libpng-1.6.40
gzip -cd ../libpng-1.6.39-apng.patch.gz | patch -p1
./configure --prefix=/usr --disable-static
make -j $PARALLEL_JOBS
make install
cd /usr/src
rm -rf libpng-1.6.40

echo "# libuv-1.46.0"
tar -zxf libuv-v1.46.0.tar.gz
cd libuv-v1.46.0
sh autogen.sh
./configure --prefix=/usr --disable-static
make -j $PARALLEL_JOBS
make install
cd /usr/src
rm -rf libuv-v1.46.0

echo "# libarchive-3.7.1"
tar -Jxf libarchive-3.7.1.tar.xz
cd libarchive-3.7.1
./configure --prefix=/usr --disable-static
make -j $PARALLEL_JOBS
make install
cd /usr/src
rm -rf libarchive-3.7.1

echo "# CMake-3.27.2"
tar -zxf cmake-3.27.2.tar.gz
cd cmake-3.27.2
sed -i '/"lib64"/s/64//' Modules/GNUInstallDirs.cmake
./bootstrap --prefix=/usr        \
            --system-libs        \
            --mandir=/share/man  \
            --no-system-jsoncpp  \
            --no-system-cppdap   \
            --no-system-librhash \
            --docdir=/share/doc/cmake-3.27.2
make -j $PARALLEL_JOBS
make install
cd /usr/src
rm -rf cmake-3.27.2

echo "# libjpeg-turbo-3.0.0"
tar -zxf libjpeg-turbo-3.0.0.tar.gz
cd libjpeg-turbo-3.0.0
mkdir build
cd build
cmake -DCMAKE_INSTALL_PREFIX=/usr \
      -DCMAKE_BUILD_TYPE=RELEASE  \
      -DENABLE_STATIC=FALSE       \
      -DCMAKE_INSTALL_DOCDIR=/usr/share/doc/libjpeg-turbo-3.0.0 \
      -DCMAKE_INSTALL_DEFAULT_LIBDIR=lib  \
      ..
make -j $PARALLEL_JOBS
make install
cd /usr/src
rm -rf libjpeg-turbo-3.0.0

echo "# Pixman-0.42.2"
tar -zxf pixman-0.42.2.tar.gz
cd pixman-0.42.2
mkdir build
cd build
meson setup --prefix=/usr --buildtype=release
ninja
ninja install
cd /usr/src
rm -rf pixman-0.42.2

echo "# GLib-2.76.4"
tar -Jxf glib-2.76.4.tar.xz
cd glib-2.76.4
patch -Np1 -i ../glib-skip_warnings-1.patch
mkdir build
cd build
meson setup ..            \
      --prefix=/usr       \
      --buildtype=release \
      -Dman=false
ninja
ninja install
cd /usr/src
rm -rf glib-2.76.4

echo "# FreeType-2.13.1"
tar -Jxf freetype-2.13.1.tar.xz
cd freetype-2.13.1
sed -ri "s:.*(AUX_MODULES.*valid):\1:" modules.cfg
sed -r "s:.*(#.*SUBPIXEL_RENDERING) .*:\1:" -i include/freetype/config/ftoption.h
./configure --prefix=/usr --enable-freetype-config --disable-static
make -j $PARALLEL_JOBS
make install
cd /usr/src
rm -rf freetype-2.13.1

echo "# Fontconfig-2.14.2"
tar -Jxf fontconfig-2.14.2.tar.xz
cd fontconfig-2.14.2
./configure --prefix=/usr        \
            --sysconfdir=/etc    \
            --localstatedir=/var \
            --disable-docs       \
            --docdir=/usr/share/doc/fontconfig-2.14.2
make -j $PARALLEL_JOBS
make install
cd /usr/src
rm -rf fontconfig-2.14.2

echo "# libdrm-2.4.115"
tar -Jxf libdrm-2.4.115.tar.xz
cd libdrm-2.4.115
mkdir build
cd build
meson setup --prefix=/usr \
            --buildtype=release   \
            -Dudev=true           \
            -Dvalgrind=disabled   \
            ..
ninja
ninja install
cd /usr/src
rm -rf libdrm-2.4.115

echo "# Wayland-1.22.0"
tar -Jxf wayland-1.22.0.tar.xz
cd wayland-1.22.0
mkdir build
cd build
meson setup ..            \
      --prefix=/usr       \
      --buildtype=release \
      -Ddocumentation=false
ninja
ninja install
cd /usr/src
rm -rf wayland-1.22.0

echo "# Wayland-Protocols-1.32"
tar -Jxf wayland-protocols-1.32.tar.xz
cd wayland-protocols-1.32
mkdir build
cd build
meson setup --prefix=/usr --buildtype=release
ninja
ninja install
cd /usr/src
rm -rf wayland-protocols-1.32

echo "# wayland-utils-1.2.0"
tar -Jxf wayland-utils-1.2.0.tar.xz
cd wayland-utils-1.2.0
mkdir build
cd build
meson setup --prefix=/usr --buildtype=release
ninja
ninja install
cd /usr/src
rm -rf wayland-utils-1.2.0

echo "# Mako-1.2.4"
tar -zxf Mako-1.2.4.tar.gz
cd Mako-1.2.4
pip3 wheel -w dist --no-build-isolation --no-deps $PWD
pip3 install --no-index --no-user --find-links dist Mako
cd /usr/src
rm -rf Mako-1.2.4

echo "# Mesa-23.1.6"
tar -Jxf mesa-23.1.6.tar.xz
cd mesa-23.1.6
mkdir build
cd build
meson setup                             \
      --prefix=/usr                     \
      --buildtype=release               \
      --sysconfdir=/etc                 \
      -Dplatforms="wayland"             \
      -Dgallium-drivers="vc4,v3d,kmsro" \
      -Dvulkan-drivers="broadcom"       \
      -Dvideo-codecs="vc1dec,h264dec,h264enc,h265dec,h265enc" \
      -Dglx=disabled                    \
      -Dvalgrind=disabled               \
      -Dlibunwind=disabled              \
      ..
ninja
ninja install
cd /usr/src
rm -rf mesa-23.1.6

echo "# Cairo-1.17.6"
tar -Jxf cairo-1.17.6.tar.xz
cd cairo-1.17.6
sed 's/PTR/void */' -i util/cairo-trace/lookup-symbol.c
sed -e "/@prefix@/a exec_prefix=@exec_prefix@" -i util/cairo-script/cairo-script-interpreter.pc.in
./configure --prefix=/usr    \
            --disable-static \
            --enable-tee     \
            --enable-glesv2  \
            --enable-egl
make -j $PARALLEL_JOBS
make install
cd /usr/src
rm -rf cairo-1.17.6

echo "# mtdev-1.1.6"
tar -jxf mtdev-1.1.6.tar.bz2
cd mtdev-1.1.6
if [[ "$RPI_MODEL" == "64" ]] ; then
    patch -Np1 -i ../mtdev-1.1.6-aarch64-fix.patch
fi
./configure --prefix=/usr --disable-static
make -j $PARALLEL_JOBS
make install
cd /usr/src
rm -rf mtdev-1.1.6

echo "# XKeyboardConfig-2.39"
tar -Jxf xkeyboard-config-2.39.tar.xz
cd xkeyboard-config-2.39
mkdir build
cd build
meson setup --prefix=/usr --buildtype=release ..
ninja
ninja install
cd /usr/src
rm -rf xkeyboard-config-2.39

echo "# libxkbcommon-1.5.0"
tar -Jxf libxkbcommon-1.5.0.tar.xz
cd libxkbcommon-1.5.0
mkdir build
cd build
meson setup ..            \
      --prefix=/usr       \
      --buildtype=release \
      -Denable-docs=false \
      -Denable-x11=false
ninja
ninja install
cd /usr/src
rm -rf libxkbcommon-1.5.0

echo "# libevdev-1.13.1"
tar -Jxf libevdev-1.13.1.tar.xz
cd libevdev-1.13.1
./configure --prefix=/usr
make -j $PARALLEL_JOBS
make install
cd /usr/src
rm -rf libevdev-1.13.1

echo "# libinput-1.21.0"
tar -Jxf libinput-1.21.0.tar.xz
cd libinput-1.21.0
mkdir build
cd build
meson setup ..              \
      --prefix=/usr         \
      --buildtype=release   \
      -Dudev-dir=/lib/udev  \
      -Ddocumentation=false \
      -Dlibwacom=false      \
      -Ddebug-gui=false     \
      -Dtests=false
ninja
ninja install
udevadm hwdb --update
cd /usr/src
rm -rf libinput-1.21.0

echo "# seatd-0.8.0"
mv 0.8.0.tar.gz seatd-0.8.0.tar.gz
tar -zxf seatd-0.8.0.tar.gz
cd seatd-0.8.0
mkdir build
cd build
meson setup ..              \
      --prefix=/usr         \
      --buildtype=release
ninja
ninja install
cd /usr/src
rm -rf seatd-0.8.0

echo "# weston-12.0.2"
wget https://raw.githubusercontent.com/vcrhonek/hwdata/master/pnp.ids -O /usr/share/hwdata/pnp.ids
tar -Jxf weston-12.0.2.tar.xz
cd weston-12.0.2
mkdir build
cd build
meson setup ..                             \
      --prefix=/usr                        \
      --buildtype=release                  \
      -Dxwayland=false                     \
      -Dbackend-x11=false                  \
      -Dimage-webp=false                   \
      -Dbackend-drm-screencast-vaapi=false \
      -Dbackend-rdp=false                  \
      -Dcolor-management-lcms=false        \
      -Dsystemd=false                      \
      -Dremoting=false                     \
      -Ddemo-clients=false                 \
      -Dpipewire=false                     \
      -Dbackend-pipewire=false             \
      -Dbackend-vnc=false
ninja
ninja install
cp -v compositor/weston.ini ~/weston.ini
echo "/dev/shm/wayland dir 1700 root root" >> /etc/sysconfig/createfiles
echo "export XDG_RUNTIME_DIR=/dev/shm/wayland" >> ~/.profile
if [[ "$RPI_MODEL" == "64" ]] ; then
    echo "export VK_ICD_FILENAMES=/usr/share/vulkan/icd.d/broadcom_icd.aarch64.json" >> ~/.profile
else
    echo "export VK_ICD_FILENAMES=/usr/share/vulkan/icd.d/broadcom_icd.armv7l.json" >> ~/.profile
fi
echo "Put seatd > /var/log/seatd.log 2>&1 & in /etc/init.d/rc.local"
cd /usr/src
rm -rf weston-12.0.2

echo "# dejavu-fonts-ttf-2.37"
tar -jxf dejavu-fonts-ttf-2.37.tar.bz2
cd dejavu-fonts-ttf-2.37
install -v -d -m755 /usr/share/fonts/dejavu
install -v -m644 ttf/*.ttf /usr/share/fonts/dejavu
fc-cache /usr/share/fonts/dejavu
cd /usr/src
rm -rf dejavu-fonts-ttf-2.37

echo -e "--------------------------------------------------------------------"
printf 'Total script time: %s\n' $(timer $total_time)
